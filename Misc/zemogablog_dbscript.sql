USE [master]
GO
/****** Object:  Database [ZemogaBlog]    Script Date: 26/09/2021 3:41:57 p. m. ******/
CREATE DATABASE [ZemogaBlog]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ZemogaBlog', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ZemogaBlog.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ZemogaBlog_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\ZemogaBlog_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [ZemogaBlog] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ZemogaBlog].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ZemogaBlog] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ZemogaBlog] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ZemogaBlog] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ZemogaBlog] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ZemogaBlog] SET ARITHABORT OFF 
GO
ALTER DATABASE [ZemogaBlog] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [ZemogaBlog] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ZemogaBlog] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ZemogaBlog] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ZemogaBlog] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ZemogaBlog] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ZemogaBlog] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ZemogaBlog] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ZemogaBlog] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ZemogaBlog] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ZemogaBlog] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ZemogaBlog] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ZemogaBlog] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ZemogaBlog] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ZemogaBlog] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ZemogaBlog] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ZemogaBlog] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ZemogaBlog] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ZemogaBlog] SET  MULTI_USER 
GO
ALTER DATABASE [ZemogaBlog] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ZemogaBlog] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ZemogaBlog] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ZemogaBlog] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ZemogaBlog] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ZemogaBlog] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [ZemogaBlog] SET QUERY_STORE = OFF
GO
USE [ZemogaBlog]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 26/09/2021 3:41:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comments]    Script Date: 26/09/2021 3:41:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comments](
	[Id] [uniqueidentifier] NOT NULL,
	[Content] [nvarchar](max) NULL,
	[Date] [datetime2](7) NOT NULL,
	[ByUserId] [uniqueidentifier] NULL,
	[Type] [int] NOT NULL,
	[PostId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Comments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Posts]    Script Date: 26/09/2021 3:41:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Posts](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	[PublishDate] [datetime2](7) NOT NULL,
	[ByUserId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Posts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 26/09/2021 3:41:58 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Role] [int] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210926192034_InitialMigration', N'5.0.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20210926193405_PostIdPropertyOnCommentsTable', N'5.0.10')
GO
INSERT [dbo].[Comments] ([Id], [Content], [Date], [ByUserId], [Type], [PostId]) VALUES (N'353a1b9c-05ff-4a77-b9a2-1a23142ba1cd', N'Nice', CAST(N'2021-06-16T05:00:00.0000000' AS DateTime2), N'edcc1ce5-b22f-4198-ba74-aefeca328f3d', 0, N'dc438ed2-e248-4646-9486-b4f096aaaa84')
INSERT [dbo].[Comments] ([Id], [Content], [Date], [ByUserId], [Type], [PostId]) VALUES (N'6abee980-c784-45a2-88a2-a24e24a186fb', N'Too Offensive', CAST(N'2021-06-18T08:00:00.0000000' AS DateTime2), N'edcc1ce5-b22f-4198-ba74-aefeca328f3d', 1, N'17df9f81-4c01-4f72-a059-9782aa4fe02e')
INSERT [dbo].[Comments] ([Id], [Content], [Date], [ByUserId], [Type], [PostId]) VALUES (N'51cf4287-8ab5-464f-9876-c00dd8a28d0d', N'Welcome', CAST(N'2021-05-16T02:00:00.0000000' AS DateTime2), N'29e08aa0-a620-49bc-944c-b09f05b3a6fa', 0, N'093ffbc3-709d-4333-85df-024ca46945e2')
INSERT [dbo].[Comments] ([Id], [Content], [Date], [ByUserId], [Type], [PostId]) VALUES (N'8f82dfa9-63d0-41b1-be63-dc8e116aade7', N'Thanks', CAST(N'2021-06-16T05:02:00.0000000' AS DateTime2), N'f8144f1c-6e00-4645-ac72-90b82dbfd26f', 0, N'dc438ed2-e248-4646-9486-b4f096aaaa84')
GO
INSERT [dbo].[Posts] ([Id], [Title], [Content], [Status], [PublishDate], [ByUserId]) VALUES (N'093ffbc3-709d-4333-85df-024ca46945e2', N'Hello World', N'this is the first post', 3, CAST(N'2021-05-16T00:00:00.0000000' AS DateTime2), N'f8144f1c-6e00-4645-ac72-90b82dbfd26f')
INSERT [dbo].[Posts] ([Id], [Title], [Content], [Status], [PublishDate], [ByUserId]) VALUES (N'6edc8388-eda9-4f36-aa96-136a598eb0df', N'Currently Editing this one', N'This post is not completed yet', 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'f8144f1c-6e00-4645-ac72-90b82dbfd26f')
INSERT [dbo].[Posts] ([Id], [Title], [Content], [Status], [PublishDate], [ByUserId]) VALUES (N'b05edab7-a22e-4e4c-a706-7d66d38754e9', N'Post # 4', N'My last post', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'f8144f1c-6e00-4645-ac72-90b82dbfd26f')
INSERT [dbo].[Posts] ([Id], [Title], [Content], [Status], [PublishDate], [ByUserId]) VALUES (N'17df9f81-4c01-4f72-a059-9782aa4fe02e', N'Post # 3', N'third one here', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'f8144f1c-6e00-4645-ac72-90b82dbfd26f')
INSERT [dbo].[Posts] ([Id], [Title], [Content], [Status], [PublishDate], [ByUserId]) VALUES (N'dc438ed2-e248-4646-9486-b4f096aaaa84', N'Post # 2', N'Second post', 3, CAST(N'2021-06-16T00:00:00.0000000' AS DateTime2), N'f8144f1c-6e00-4645-ac72-90b82dbfd26f')
GO
INSERT [dbo].[Users] ([Id], [Name], [Role]) VALUES (N'f8144f1c-6e00-4645-ac72-90b82dbfd26f', N'writer-user', 1)
INSERT [dbo].[Users] ([Id], [Name], [Role]) VALUES (N'edcc1ce5-b22f-4198-ba74-aefeca328f3d', N'editor-user', 2)
INSERT [dbo].[Users] ([Id], [Name], [Role]) VALUES (N'29e08aa0-a620-49bc-944c-b09f05b3a6fa', N'public-user', 0)
GO
/****** Object:  Index [IX_Comments_ByUserId]    Script Date: 26/09/2021 3:42:02 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_Comments_ByUserId] ON [dbo].[Comments]
(
	[ByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Comments_PostId]    Script Date: 26/09/2021 3:42:02 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_Comments_PostId] ON [dbo].[Comments]
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_Posts_ByUserId]    Script Date: 26/09/2021 3:42:02 p. m. ******/
CREATE NONCLUSTERED INDEX [IX_Posts_ByUserId] ON [dbo].[Posts]
(
	[ByUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Posts_PostId] FOREIGN KEY([PostId])
REFERENCES [dbo].[Posts] ([Id])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_Posts_PostId]
GO
ALTER TABLE [dbo].[Comments]  WITH CHECK ADD  CONSTRAINT [FK_Comments_Users_ByUserId] FOREIGN KEY([ByUserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Comments] CHECK CONSTRAINT [FK_Comments_Users_ByUserId]
GO
ALTER TABLE [dbo].[Posts]  WITH CHECK ADD  CONSTRAINT [FK_Posts_Users_ByUserId] FOREIGN KEY([ByUserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Posts] CHECK CONSTRAINT [FK_Posts_Users_ByUserId]
GO
USE [master]
GO
ALTER DATABASE [ZemogaBlog] SET  READ_WRITE 
GO
