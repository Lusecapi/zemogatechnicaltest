## Prerequisites
- OS Windows 10
- .Net Core 3.1 (https://dotnet.microsoft.com/download/dotnet/3.1) Or (Installing VS Studio 2019 ASP.NET & Web Development module)
- .Net Core Web Hosting Bundle (https://dotnet.microsoft.com/download/dotnet/thank-you/runtime-aspnetcore-5.0.10-windows-hosting-bundle-installer)
- IIS (https://luisperis.com/instalar-iis-windows-10-5-minutos/)
- SQL Server 2019 (https://www.microsoft.com/es-es/sql-server/sql-server-downloads)
- SQL Server Management Studio (https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15)
- Postman (https://www.postman.com/downloads/)

## Solution Set Up
After installing all the prerequisites, we need to create a new application on our local IIS.
This is a good guide that shows you how to set it up (https://www.youtube.com/watch?v=XMPxHPXYnIA).

For a quick setup, The Web API build files are inside the output_build folder in the repository root directory, use these files for creating the web application on IIS.

Then we need to create tha Database for the Web API in SQL Server using the script inside Misc folder.

And finally Test the Web API using the Postman collection with the Requests configuration.

### Step by Step:
1. Create a new folder in this path 'C:\inetpub\wwwroot'
2. Copy all the files inside *output_build* folder and paste them inside the new folder created above.
3. Open IIS and create a new Application Pool
4. Create a new Web Site and set a name, the application pool you just created, the phisical path of the folder crated in step 1 and a port (90 for example)
5. The Web API should be running.
6. Use the db script that is inside the Misc folder on the repository root directory to create the database for the Web API in your SQL Server instance.
7. Update the _Connection String_ on the Web API appsettings.json file that is inside the folder created in step 1 to match your database configuration.
8. Use the Postman collection file that is inside the Misc folder on the repository root directory to import the collection on your Postman client for testing the WebAPI.
9. Setup the Postman collection variables to match your machine configuration.
10. Test.

## Login
This solution uses JWT as Authentication system. You need to Authenticate first on the Web API to get the users access token. Then you need to add this token on every other requests you make to the Web API (Inside Request header).

In the postman collection setup you just need to set a viariable after you generate the token. This token has a default expiration time of 90 minutes, you can edit this in the _appsettings.json_ Wep API file.

### Credentials
- Public User = { "UserName": "public-user", "Password": "p123"}
- Writer User = { "UserName": "writer-user", "Password": "w123"}
- Editor User = { "UserName": "editor-user", "Password": "e123"}


### Types Values to send in requests
- Comment Type => Normal = 0, Rejection = 1 (Used in Add Comment request. Needs to be send as numeric value instead of string)




### This solution tooks me about 18 hours to complete it.
