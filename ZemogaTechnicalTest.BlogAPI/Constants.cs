﻿namespace ZemogaTechnicalTest.BlogAPI
{
    internal static class Constants
    {
        internal static class AppSettingsKeyNames
        {
            internal const string SECRET_KEY_NAME = "SecretKey";
            internal const string TOKEN_EXPIRATION_TIME_NAME = "TokenExpirationMinutes";
            internal const string CONNECTION_STRING_NAME = "ZemogaBlogDb";
        }

        internal static class AuthRoles
        {
            //Roles here must match with int value of ZemogaTechnicalTest.BlogCore.Enums.UserRole enum values

            internal const string PUBLIC = "0";
            internal const string WRITER = "1";
            internal const string EDITOR = "2";
        }
    }
}
