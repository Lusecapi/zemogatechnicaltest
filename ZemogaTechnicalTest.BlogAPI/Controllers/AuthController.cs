﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ZemogaTechnicalTest.BlogAPI.Interfaces;
using ZemogaTechnicalTest.BlogAPI.Models;
using ZemogaTechnicalTest.BlogCore.Models;
using ZemogaTechnicalTest.BlogDAL.Models;

namespace ZemogaTechnicalTest.BlogAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private const string DEFAULT_TOKEN_EXPIRATION_MINUTES = "10";

        private readonly IConfiguration config;
        private readonly IAuthService authService;
        private readonly IBlogRepo blogRepo;


        private double TokenExpirationTime => double.Parse(config.GetValue<string>(Constants.AppSettingsKeyNames.TOKEN_EXPIRATION_TIME_NAME, DEFAULT_TOKEN_EXPIRATION_MINUTES));


        public AuthController(IConfiguration _config, IAuthService _authService, IBlogRepo _blogRepo)
        {
            config = _config;
            authService = _authService;
            blogRepo = _blogRepo;
        }



        [HttpPost]
        [Route("[action]")]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] AuthInfo authInfo)
        {
            if (authInfo == null) return BadRequest();
            if (string.IsNullOrEmpty(authInfo.UserName) || string.IsNullOrEmpty(authInfo.Password)) return BadRequest();

            string userId = authService.Authenticate(authInfo.UserName, authInfo.Password);

            if (string.IsNullOrEmpty(userId)) return Unauthorized(new { msg = "User not found in auth service" });

            User user = blogRepo.GetUserById(userId);

            if (user == null) return Unauthorized(new { msg = "User not registered in blog" });

            Claim[] claims = new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Name),
                new Claim(ClaimTypes.Role, ((int)user.Role).ToString())
            };

            var key = Encoding.ASCII.GetBytes(config.GetValue<string>(Constants.AppSettingsKeyNames.SECRET_KEY_NAME));

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(TokenExpirationTime),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);

            string tokenString = tokenHandler.WriteToken(createdToken);

            if (string.IsNullOrEmpty(tokenString)) return Unauthorized(new { msg = "Token is null" });

            return Ok(new { token = tokenString, validTo = createdToken.ValidTo });
        }
    }
}
