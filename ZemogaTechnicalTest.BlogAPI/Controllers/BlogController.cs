﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using ZemogaTechnicalTest.BlogAPI.Models;
using ZemogaTechnicalTest.BlogCore.Enums;
using ZemogaTechnicalTest.BlogCore.Models;
using ZemogaTechnicalTest.BlogDAL.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ZemogaTechnicalTest.BlogAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IBlogRepo blogRepo;


        public BlogController(IBlogRepo _blogRepo)
        {
            blogRepo = _blogRepo;
        }


        private string GetAuthClaim(string claimType)
        {
            string value = string.Empty;

            try
            {
                if (User != null)
                    value = User.Claims.Where(c => c.Type == claimType).FirstOrDefault()?.Value;
            }
            catch (Exception)
            {
            }

            return value;
        }


        [HttpGet]
        [Authorize]
        public IActionResult GetPublishedPosts()
        {
            try
            {
                string userRoleCode = GetAuthClaim(ClaimTypes.Role);

                if (string.IsNullOrEmpty(userRoleCode)) return Unauthorized(new { msg = "User role is not valid" });

                if (!int.TryParse(userRoleCode, out int userRole)) throw new Exception("Role not valid");

                UserRole currenUserRole = (UserRole)userRole;

                IEnumerable<Post> publishedPosts = null;

                switch (currenUserRole)
                {
                    case UserRole.Writer:
                        publishedPosts = blogRepo.GetPostsByStatus(BlogCore.Enums.PostStatus.Published);
                        break;
                    default:
                        publishedPosts = blogRepo.GetPostsByStatus(BlogCore.Enums.PostStatus.Published)
                            .Select(p => new Post { Id = p.Id, Title = p.Title, Content = p.Content, Status = p.Status, PublishDate = p.PublishDate, ByUser = p.ByUser, Comments = p.Comments?.Where(c => c.Type != CommentType.Rejection).ToList() });
                        break;
                }

                return Ok(new { publishedPosts });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }

        [HttpPost]
        [Route("comment")]
        [Authorize]
        public IActionResult PostComment([FromBody] ReqComment comment)
        {
            if (comment == null) return BadRequest(new { msg = "Comment is null" });
            if (string.IsNullOrEmpty(comment.Content) || string.IsNullOrEmpty(comment.PostId)) return BadRequest(new { msg = "Comment properties can not be empty" });

            try
            {
                string userId = GetAuthClaim(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(userId)) return Unauthorized();

                string userRoleCode = GetAuthClaim(ClaimTypes.Role);
                
                if (string.IsNullOrEmpty(userRoleCode)) return Unauthorized();

                if (!int.TryParse(userRoleCode, out int userRole)) throw new Exception("Role not valid");

                //Rejection comment solo es valido para usuarios editor
                if ((UserRole)userRole != UserRole.Editor && comment.Type != CommentType.Normal) throw new Exception("Comment not valid");

                Comment addedComment = blogRepo.AddComment(comment.PostId, userId, comment.Content, comment.Type);

                if (addedComment == null) throw new Exception($"Could not add comment to post {addedComment}");

                return Ok(new { comment.PostId, comment = addedComment });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }


        [HttpGet]
        [Route("myposts")]
        [Authorize(Roles = Constants.AuthRoles.WRITER)]
        public IActionResult GetUserPosts()
        {

            try
            {
                string userId = GetAuthClaim(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(userId)) return Unauthorized();

                IEnumerable<Post> posts = blogRepo.GetUserPosts(userId);

                return Ok(new { posts });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }

        [HttpPost]
        [Route("posts")]
        [Authorize(Roles = Constants.AuthRoles.WRITER)]
        public IActionResult CreatePost([FromBody] ReqPost post)
        {
            if (post == null) return BadRequest(new { msg = "post can not be null" });
            if (string.IsNullOrEmpty(post.Title) || string.IsNullOrEmpty(post.Content)) return BadRequest(new { msg = "post properties can not be empty or null" });

            try
            {
                string userId = GetAuthClaim(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(userId)) return Unauthorized();

                Post addedPost = blogRepo.AddPost(userId, post.Title, post.Content);

                if (addedPost == null) throw new Exception($"Could not create post");

                return Ok(new { post = addedPost });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }

        [HttpPut]
        [Route("posts")]
        [Authorize(Roles = Constants.AuthRoles.WRITER)]
        public IActionResult EditPost(string postId, [FromBody] ReqPost post)
        {
            if (string.IsNullOrEmpty(postId)) return BadRequest(new { msg = "postId can not be null or empty" });
            if (post == null) return BadRequest(new { msg = "post can not be null" });
            if (string.IsNullOrEmpty(post.Title) && string.IsNullOrEmpty(post.Content)) return BadRequest(new { msg = "post properties can not be empty or null" });

            try
            {
                string userId = GetAuthClaim(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(userId)) return Unauthorized();

                Post original = blogRepo.GetPostById(postId);

                if (original.IsLocked) throw new InvalidOperationException("This post can not be edited");

                //Validar que el post a editar sea un post del usuario autenticado
                if (original.ByUser.Id.ToString() != userId) return Unauthorized();

                if (!blogRepo.UpdatePost(postId, post.Title, post.Content)) throw new Exception($"Could not create post");

                return Ok(new { post = blogRepo.GetPostById(postId) });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }

        [HttpPut]
        [Route("submit")]
        [Authorize(Roles = Constants.AuthRoles.WRITER)]
        public IActionResult SubmitPost(string postId)
        {
            if (string.IsNullOrEmpty(postId)) return BadRequest(new { msg = "postId is empty or null" });

            try
            {
                string userId = GetAuthClaim(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(userId)) return Unauthorized();

                Post post = blogRepo.GetPostById(postId);

                if (post == null) return NotFound(new { msg = $"Post with id {postId} not found" });

                //Validar que el post a editar sea un post del usuario autenticado
                if (post.ByUser.Id.ToString() != userId) return Unauthorized();

                if (post.Status != BlogCore.Enums.PostStatus.Draft) return BadRequest(new { msg = "The post status is not draft" });

                if (!blogRepo.UpdatePostStatus(postId, BlogCore.Enums.PostStatus.Pending)) throw new Exception("Could not update post status");

                return Ok(new { post });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }


        [HttpGet]
        [Route("pending")]
        [Authorize(Roles = Constants.AuthRoles.EDITOR)]
        public IActionResult GetPendingPosts()
        {
            try
            {
                IEnumerable<Post> pendingPosts = blogRepo.GetPostsByStatus(BlogCore.Enums.PostStatus.Pending)
                            .Select(p => new Post { Id = p.Id, Title = p.Title, Content = p.Content, Status = p.Status, PublishDate = p.PublishDate, ByUser = p.ByUser, Comments = p.Comments?.Where(c => c.Type != CommentType.Rejection).ToList() });

                return Ok(new { pendingPosts });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }


        [HttpPut]
        [Route("approve")]
        [Authorize(Roles = Constants.AuthRoles.EDITOR)]
        public IActionResult ApprovePost(string postId)
        {
            if (string.IsNullOrEmpty(postId)) return BadRequest(new { msg = "postId is empty or null" });

            try
            {
                string userId = GetAuthClaim(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(userId)) return Unauthorized();

                Post post = blogRepo.GetPostById(postId);
                
                if (post == null) return NotFound(new { msg = $"Post with id {postId} not found" });

                if (post.Status != BlogCore.Enums.PostStatus.Pending) return BadRequest(new { msg = "The post status is not pending" });

                //User must be Editor
                if (!blogRepo.UpdatePostStatus(postId, userId, true)) throw new Exception("Could not update post status");

                return Ok(new { post });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }

        [HttpPut]
        [Route("reject")]
        [Authorize(Roles = Constants.AuthRoles.EDITOR)]
        public IActionResult RejectPost([FromBody]ReqReject reject)
        {
            if (reject == null) return BadRequest(new { msg = "reject info can not be null" });
            if (string.IsNullOrEmpty(reject.PostId)) return BadRequest(new { msg = "postId is empty or null" });

            try
            {
                string userId = GetAuthClaim(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(userId)) return Unauthorized();

                Post post = blogRepo.GetPostById(reject.PostId);

                if (post == null) return NotFound(new { msg = $"Post with id {reject.PostId} not found" });

                if (post.Status != BlogCore.Enums.PostStatus.Pending) return BadRequest(new { msg = "The post status is not pending" });

                //User must be editor
                if (!blogRepo.UpdatePostStatus(reject.PostId, userId, false, reject.RejectionMessage)) throw new Exception("Could not update post status");

                return Ok(new { post });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { error = e.Message });
            }
        }


    }
}
