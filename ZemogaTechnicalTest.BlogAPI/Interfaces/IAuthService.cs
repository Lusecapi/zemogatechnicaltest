﻿namespace ZemogaTechnicalTest.BlogAPI.Interfaces
{
    public interface IAuthService
    {
        string Authenticate(string userName, string password);
    }
}
