﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZemogaTechnicalTest.BlogAPI.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Role = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    PublishDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ByUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Users_ByUserId",
                        column: x => x.ByUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ByUserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Type = table.Column<int>(type: "int", nullable: false),
                    PostId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Posts_PostId",
                        column: x => x.PostId,
                        principalTable: "Posts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Comments_Users_ByUserId",
                        column: x => x.ByUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name", "Role" },
                values: new object[] { new Guid("29e08aa0-a620-49bc-944c-b09f05b3a6fa"), "public-user", 0 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name", "Role" },
                values: new object[] { new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"), "writer-user", 1 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name", "Role" },
                values: new object[] { new Guid("edcc1ce5-b22f-4198-ba74-aefeca328f3d"), "editor-user", 2 });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "ByUserId", "Content", "Date", "PostId", "Type" },
                values: new object[,]
                {
                    { new Guid("51cf4287-8ab5-464f-9876-c00dd8a28d0d"), new Guid("29e08aa0-a620-49bc-944c-b09f05b3a6fa"), "Welcome", new DateTime(2021, 5, 16, 2, 0, 0, 0, DateTimeKind.Unspecified), null, 0 },
                    { new Guid("8f82dfa9-63d0-41b1-be63-dc8e116aade7"), new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"), "Thanks", new DateTime(2021, 6, 16, 5, 2, 0, 0, DateTimeKind.Unspecified), null, 0 },
                    { new Guid("353a1b9c-05ff-4a77-b9a2-1a23142ba1cd"), new Guid("edcc1ce5-b22f-4198-ba74-aefeca328f3d"), "Nice", new DateTime(2021, 6, 16, 5, 0, 0, 0, DateTimeKind.Unspecified), null, 0 },
                    { new Guid("6abee980-c784-45a2-88a2-a24e24a186fb"), new Guid("edcc1ce5-b22f-4198-ba74-aefeca328f3d"), "Too Offensive", new DateTime(2021, 6, 18, 8, 0, 0, 0, DateTimeKind.Unspecified), null, 1 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "ByUserId", "Content", "PublishDate", "Status", "Title" },
                values: new object[,]
                {
                    { new Guid("093ffbc3-709d-4333-85df-024ca46945e2"), new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"), "this is the first post", new DateTime(2021, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Hello World" },
                    { new Guid("dc438ed2-e248-4646-9486-b4f096aaaa84"), new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"), "Second post", new DateTime(2021, 6, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), 3, "Post # 2" },
                    { new Guid("6edc8388-eda9-4f36-aa96-136a598eb0df"), new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"), "This post is not completed yet", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 0, "Currently Editing this one" },
                    { new Guid("b05edab7-a22e-4e4c-a706-7d66d38754e9"), new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"), "My last post", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, "Post # 4" },
                    { new Guid("17df9f81-4c01-4f72-a059-9782aa4fe02e"), new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"), "third one here", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, "Post # 3" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_ByUserId",
                table: "Comments",
                column: "ByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_PostId",
                table: "Comments",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ByUserId",
                table: "Posts",
                column: "ByUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
