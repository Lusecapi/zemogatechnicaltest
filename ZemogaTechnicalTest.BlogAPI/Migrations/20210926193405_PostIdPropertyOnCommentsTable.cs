﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZemogaTechnicalTest.BlogAPI.Migrations
{
    public partial class PostIdPropertyOnCommentsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("353a1b9c-05ff-4a77-b9a2-1a23142ba1cd"),
                column: "PostId",
                value: new Guid("dc438ed2-e248-4646-9486-b4f096aaaa84"));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("51cf4287-8ab5-464f-9876-c00dd8a28d0d"),
                column: "PostId",
                value: new Guid("093ffbc3-709d-4333-85df-024ca46945e2"));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("6abee980-c784-45a2-88a2-a24e24a186fb"),
                column: "PostId",
                value: new Guid("17df9f81-4c01-4f72-a059-9782aa4fe02e"));

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("8f82dfa9-63d0-41b1-be63-dc8e116aade7"),
                column: "PostId",
                value: new Guid("dc438ed2-e248-4646-9486-b4f096aaaa84"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("353a1b9c-05ff-4a77-b9a2-1a23142ba1cd"),
                column: "PostId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("51cf4287-8ab5-464f-9876-c00dd8a28d0d"),
                column: "PostId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("6abee980-c784-45a2-88a2-a24e24a186fb"),
                column: "PostId",
                value: null);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: new Guid("8f82dfa9-63d0-41b1-be63-dc8e116aade7"),
                column: "PostId",
                value: null);
        }
    }
}
