﻿namespace ZemogaTechnicalTest.BlogAPI.Models
{
    public class AuthInfo
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
