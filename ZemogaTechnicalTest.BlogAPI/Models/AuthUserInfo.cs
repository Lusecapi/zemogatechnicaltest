﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZemogaTechnicalTest.BlogAPI.Models
{
    public class AuthUserInfo
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string UserId { get; set; }
    }
}
