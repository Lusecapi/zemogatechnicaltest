﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZemogaTechnicalTest.BlogAPI.Interfaces;
using ZemogaTechnicalTest.BlogCore.Models;

namespace ZemogaTechnicalTest.BlogAPI.Models
{
    public class FakeAuthService : IAuthService
    {
        private readonly AuthUserInfo[] users = new AuthUserInfo[]
        {
            new AuthUserInfo
            {
                UserName = "public-user",
                Password = "p123",
                UserId = "29e08aa0-a620-49bc-944c-b09f05b3a6fa"
            },
            new AuthUserInfo
            {
                UserName = "writer-user",
                Password = "w123",
                UserId = "f8144f1c-6e00-4645-ac72-90b82dbfd26f"
            },
            new AuthUserInfo
            {
                UserName = "editor-user",
                Password = "e123",
                UserId = "edcc1ce5-b22f-4198-ba74-aefeca328f3d"
            }
        };

        

        public string Authenticate(string userName, string password)
        {
            return users
                .Where(u => u.UserName == userName)
                .Where(u => u.Password == password)
                .Select(u => u.UserId)
                .FirstOrDefault();
        }
    }
}
