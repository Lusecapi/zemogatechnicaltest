﻿using ZemogaTechnicalTest.BlogCore.Enums;

namespace ZemogaTechnicalTest.BlogAPI.Models
{
    public class ReqComment
    {
        public string PostId { get; set; }
        public string Content { get; set; }
        public CommentType Type { get; set; }
    }
}
