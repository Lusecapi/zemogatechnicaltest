﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZemogaTechnicalTest.BlogAPI.Models
{
    public class ReqPost
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
