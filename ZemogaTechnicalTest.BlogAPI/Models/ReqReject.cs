﻿namespace ZemogaTechnicalTest.BlogAPI.Models
{
    public class ReqReject
    {
        public string PostId { get; set; }
        public string RejectionMessage { get; set; }
    }
}
