using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using ZemogaTechnicalTest.BlogAPI.Interfaces;
using ZemogaTechnicalTest.BlogAPI.Models;
using ZemogaTechnicalTest.BlogDAL.Data;
using ZemogaTechnicalTest.BlogDAL.Models;
using ZemogaTechnicalTest.BlogDAL.Repositories;

namespace ZemogaTechnicalTest.BlogAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IAuthService, FakeAuthService>();

            //services.AddScoped<IBlogRepo, FakeBlogRepo>();
            services.AddDbContext<BlogSqlDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString(Constants.AppSettingsKeyNames.CONNECTION_STRING_NAME),
                                                                                        b => b.MigrationsAssembly(typeof(Startup).Assembly.GetName().Name)));
            services.AddScoped<IBlogRepo, SqlServerBlogRepo>();

            var key = Encoding.ASCII.GetBytes(Configuration.GetValue<string>(Constants.AppSettingsKeyNames.SECRET_KEY_NAME));

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
