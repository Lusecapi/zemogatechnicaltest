﻿namespace ZemogaTechnicalTest.BlogCore.Enums
{
    public enum CommentType
    {
        Normal = 0,
        Rejection = 1
    }
}
