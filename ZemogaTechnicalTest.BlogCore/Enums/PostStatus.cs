﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZemogaTechnicalTest.BlogCore.Enums
{
    public enum PostStatus
    {
        Draft = 0,
        Pending = 1,
        Rejected = 2,
        Published = 3,
    }
}
