﻿namespace ZemogaTechnicalTest.BlogCore.Enums
{
    public enum UserRole
    {
        Public = 0,
        Writer = 1,
        Editor = 2
    }
}
