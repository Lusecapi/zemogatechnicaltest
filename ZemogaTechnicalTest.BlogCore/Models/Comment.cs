﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ZemogaTechnicalTest.BlogCore.Enums;

namespace ZemogaTechnicalTest.BlogCore.Models
{
    public class Comment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public User ByUser { get; set; }
        public CommentType Type { get; set; }
    }
}