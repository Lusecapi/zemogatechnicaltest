﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ZemogaTechnicalTest.BlogCore.Enums;

namespace ZemogaTechnicalTest.BlogCore.Models
{
    public class Post
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public PostStatus Status { get; set; }
        public DateTime PublishDate { get; set; }
        public User ByUser { get; set; }
        public List<Comment> Comments { get; set; }

        public bool IsLocked => Status == PostStatus.Published || Status == PostStatus.Pending;
    }
}
