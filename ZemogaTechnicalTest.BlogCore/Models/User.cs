﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ZemogaTechnicalTest.BlogCore.Enums;

namespace ZemogaTechnicalTest.BlogCore.Models
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public UserRole Role { get; set; }
    }
}
