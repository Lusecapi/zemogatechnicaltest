﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ZemogaTechnicalTest.BlogCore.Enums;
using ZemogaTechnicalTest.BlogCore.Models;

namespace ZemogaTechnicalTest.BlogDAL.Data
{
    public class BlogSqlDbContext : DbContext
    {


        public BlogSqlDbContext(DbContextOptions<BlogSqlDbContext> options) : base(options)
        {

        }


        public DbSet<User> Users { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Post> Posts { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            User[] defaultUsers = new User[]
            {
                new User
                {
                    Id = new Guid("29e08aa0-a620-49bc-944c-b09f05b3a6fa"),
                    Name = "public-user",
                    Role = UserRole.Public
                },
                new User
                {
                    Id = new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"),
                    Name = "writer-user",
                    Role = UserRole.Writer
                },
                new User
                {
                    Id = new Guid("edcc1ce5-b22f-4198-ba74-aefeca328f3d"),
                    Name = "editor-user",
                    Role = UserRole.Editor
                }
            };


            Comment[] defaultComments = new Comment[]
            {
                new Comment
                {
                    Id = new Guid("51cf4287-8ab5-464f-9876-c00dd8a28d0d"),
                    Content = "Welcome",
                    ByUser = defaultUsers[0],
                    Date = DateTime.ParseExact("2021-05-16", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(2),
                    Type = CommentType.Normal
                },
                new Comment
                {
                    Id = new Guid("353a1b9c-05ff-4a77-b9a2-1a23142ba1cd"),
                    Content = "Nice",
                    ByUser = defaultUsers[2],
                    Date = DateTime.ParseExact("2021-06-16", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(5),
                    Type = CommentType.Normal
                },
                new Comment
                {
                    Id = new Guid("8f82dfa9-63d0-41b1-be63-dc8e116aade7"),
                    Content = "Thanks",
                    ByUser = defaultUsers[1],
                    Date = DateTime.ParseExact("2021-06-16", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(5).AddMinutes(2),
                    Type = CommentType.Normal
                },
                new Comment
                {
                    Id = new Guid("6abee980-c784-45a2-88a2-a24e24a186fb"),
                    Content = "Too Offensive",
                    ByUser = defaultUsers[2],
                    Date = DateTime.ParseExact("2021-06-18", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(8),
                    Type = CommentType.Rejection
                }
            };

            Post[] defaultPosts = new Post[]
            {
                new Post
                {
                    Id = new Guid("093ffbc3-709d-4333-85df-024ca46945e2"),
                    Title = "Hello World",
                    Content = "this is the first post",
                    PublishDate = DateTime.ParseExact("2021-05-16", "yyyy-MM-dd", CultureInfo.InvariantCulture),
                    Comments = new List<Comment>()
                    {
                        defaultComments[0]
                    },
                    Status = PostStatus.Published,
                    ByUser = defaultUsers[1],
                },
                new Post
                {
                    Id = new Guid("dc438ed2-e248-4646-9486-b4f096aaaa84"),
                    Title = "Post # 2",
                    Content = "Second post",
                    PublishDate = DateTime.ParseExact("2021-06-16", "yyyy-MM-dd", CultureInfo.InvariantCulture),
                    Comments = new List<Comment>()
                    {
                        defaultComments[1],
                        defaultComments[2]
                    },
                    Status = PostStatus.Published,
                    ByUser = defaultUsers[1],
                },
                new Post
                {
                    Id = new Guid("6edc8388-eda9-4f36-aa96-136a598eb0df"),
                    Title = "Currently Editing this one",
                    Content = "This post is not completed yet",
                    Status = PostStatus.Draft,
                    ByUser = defaultUsers[1],
                },
                new Post
                {
                    Id = new Guid("b05edab7-a22e-4e4c-a706-7d66d38754e9"),
                    Title = "Post # 4",
                    Content = "My last post",
                    Status = PostStatus.Pending,
                    ByUser = defaultUsers[1],
                },
                new Post
                {
                    Id = new Guid("17df9f81-4c01-4f72-a059-9782aa4fe02e"),
                    Title = "Post # 3",
                    Content = "third one here",
                    Status = PostStatus.Rejected,
                    ByUser = defaultUsers[1],
                    Comments = new List<Comment>()
                    {
                        defaultComments[3]
                    }
                }
            };



            modelBuilder.Entity<User>().HasData(defaultUsers);
            modelBuilder.Entity<Post>().HasData
                (
                    defaultPosts
                    .Select(p => new { p.Id, p.Title, p.Content, p.Status, p.PublishDate, ByUserId = p.ByUser.Id })
                    .ToArray()
                );
            modelBuilder.Entity<Comment>().HasData
                (
                    defaultComments
                    .Select(c => new { c.Id, c.Content, c.Date, c.Type, ByUserId = c.ByUser.Id, PostId = defaultPosts.Where(p => p.Comments?.Exists(pc => pc.Id == c.Id) == true).FirstOrDefault()?.Id })
                    .ToArray()
                );


            base.OnModelCreating(modelBuilder);
        }

    }
}
