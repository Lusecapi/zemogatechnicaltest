﻿using System;
using System.Collections.Generic;
using System.Text;
using ZemogaTechnicalTest.BlogCore.Enums;
using ZemogaTechnicalTest.BlogCore.Models;

namespace ZemogaTechnicalTest.BlogDAL.Models
{
    public interface IBlogRepo
    {
        User GetUserById(string userId);
        Post GetPostById(string postId);
        IEnumerable<Post> GetPostsByStatus(PostStatus status);
        Comment AddComment(string postId, string userId, string content, CommentType type);
        IEnumerable<Post> GetUserPosts(string userId);

        Post AddPost(string userId, string title, string content);
        bool UpdatePost(string postId, string title, string content);
        bool UpdatePostStatus(string postId, PostStatus status);
        bool UpdatePostStatus(string postId, string byUserId, bool approve, string rejectionMessage = null);
    }
}
