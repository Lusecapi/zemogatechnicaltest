﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ZemogaTechnicalTest.BlogCore.Enums;
using ZemogaTechnicalTest.BlogCore.Models;
using ZemogaTechnicalTest.BlogDAL.Models;

namespace ZemogaTechnicalTest.BlogDAL.Repositories
{
    public class FakeBlogRepo : IBlogRepo
    {
        private readonly List<User> users;
        private readonly List<Post> posts;
        private readonly List<Comment> comments;

        public FakeBlogRepo()
        {
            users = new List<User>()
            {
                new User
                {
                    Id = new Guid("29e08aa0-a620-49bc-944c-b09f05b3a6fa"),
                    Name = "public-user",
                    Role = UserRole.Public
                },
                new User
                {
                    Id = new Guid("f8144f1c-6e00-4645-ac72-90b82dbfd26f"),
                    Name = "writer-user",
                    Role = UserRole.Writer
                },
                new User
                {
                    Id = new Guid("edcc1ce5-b22f-4198-ba74-aefeca328f3d"),
                    Name = "editor-user",
                    Role = UserRole.Editor
                }
            };
            comments = new List<Comment>()
            {
                new Comment
                {
                    Id = new Guid("51cf4287-8ab5-464f-9876-c00dd8a28d0d"),
                    Content = "Welcome",
                    ByUser = users[0],
                    Date = DateTime.ParseExact("2021-05-16", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(2),
                    Type = CommentType.Normal
                },
                new Comment
                {
                    Id = new Guid("353a1b9c-05ff-4a77-b9a2-1a23142ba1cd"),
                    Content = "Nice",
                    ByUser = users[2],
                    Date = DateTime.ParseExact("2021-06-16", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(5),
                    Type = CommentType.Normal
                },
                new Comment
                {
                    Id = new Guid("8f82dfa9-63d0-41b1-be63-dc8e116aade7"),
                    Content = "Thanks",
                    ByUser = users[1],
                    Date = DateTime.ParseExact("2021-06-16", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(5).AddMinutes(2),
                    Type = CommentType.Normal
                },
                new Comment
                {
                    Id = new Guid("6abee980-c784-45a2-88a2-a24e24a186fb"),
                    Content = "Too Offensive",
                    ByUser = users[2],
                    Date = DateTime.ParseExact("2021-06-18", "yyyy-MM-dd", CultureInfo.InvariantCulture).AddHours(8),
                    Type = CommentType.Rejection
                }
            };
            posts = new List<Post>()
            {
                new Post
                {
                    Id = new Guid("093ffbc3-709d-4333-85df-024ca46945e2"),
                    Title = "Hello World",
                    Content = "this is the first post",
                    PublishDate = DateTime.ParseExact("2021-05-16", "yyyy-MM-dd", CultureInfo.InvariantCulture),
                    Comments = new List<Comment>()
                    {
                        comments[0]
                    },
                    Status = PostStatus.Published,
                    ByUser = users[1],
                },
                new Post
                {
                    Id = new Guid("dc438ed2-e248-4646-9486-b4f096aaaa84"),
                    Title = "Post # 2",
                    Content = "Second post",
                    PublishDate = DateTime.ParseExact("2021-06-16", "yyyy-MM-dd", CultureInfo.InvariantCulture),
                    Comments = new List<Comment>()
                    {
                        comments[1],
                        comments[2]
                    },
                    Status = PostStatus.Published,
                    ByUser = users[1],
                },
                new Post
                {
                    Id = new Guid("6edc8388-eda9-4f36-aa96-136a598eb0df"),
                    Title = "Currently Editing this one",
                    Content = "This post is not completed yet",
                    Status = PostStatus.Draft,
                    ByUser = users[1],
                },
                new Post
                {
                    Id = new Guid("b05edab7-a22e-4e4c-a706-7d66d38754e9"),
                    Title = "Post # 4",
                    Content = "My last post",
                    Status = PostStatus.Pending,
                    ByUser = users[1],
                },
                new Post
                {
                    Id = new Guid("17df9f81-4c01-4f72-a059-9782aa4fe02e"),
                    Title = "Post # 3",
                    Content = "third one here",
                    Status = PostStatus.Rejected,
                    ByUser = users[1],
                    Comments = new List<Comment>()
                    {
                        comments[3]
                    }
                }
            };
        }

        public Comment AddComment(string postId, string userId, string content, CommentType type)
        {
            throw new NotImplementedException();
        }

        public Post AddPost(string userId, string title, string content)
        {
            throw new NotImplementedException();
        }

        public Post GetPostById(string postId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> GetPostsByStatus(PostStatus status)
        {
            throw new NotImplementedException();
        }

        public User GetUserById(string userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> GetUserPosts(string userId)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePost(string postId, string title, string content)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePostStatus(string postId, PostStatus status)
        {
            throw new NotImplementedException();
        }

        public bool UpdatePostStatus(string postId, string byUserId, bool approve, string rejectionMessage = null)
        {
            throw new NotImplementedException();
        }
    }
}
