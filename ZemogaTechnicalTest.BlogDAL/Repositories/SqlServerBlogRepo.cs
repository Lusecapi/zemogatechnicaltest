﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ZemogaTechnicalTest.BlogCore.Enums;
using ZemogaTechnicalTest.BlogCore.Models;
using ZemogaTechnicalTest.BlogDAL.Data;
using ZemogaTechnicalTest.BlogDAL.Models;

namespace ZemogaTechnicalTest.BlogDAL.Repositories
{
    public class SqlServerBlogRepo : IBlogRepo
    {
        private readonly BlogSqlDbContext context;


        public SqlServerBlogRepo(BlogSqlDbContext _context)
        {
            context = _context;
        }



        public Comment AddComment(string postId, string userId, string content, CommentType type)
        {
            try
            {
                Comment commentToInsert = new Comment
                {
                    Content = content,
                    ByUser = GetUserById(userId),
                    Date = DateTime.Now,
                    Type = type
                };

                Comment insertedComment = context
                    .Comments
                    .Add(commentToInsert)
                    .Entity;

                Post post = GetPostById(postId);

                if (post == null) return null;

                if (post.Comments == null)
                    post.Comments = new List<Comment>();

                post.Comments.Add(insertedComment);

                context.SaveChanges();

                return insertedComment;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Post AddPost(string userId, string title, string content)
        {
            try
            {
                Post postToInsert = new Post
                {
                    Title = title,
                    Content = content,
                    Status = PostStatus.Draft,
                    ByUser = GetUserById(userId)
                };

                Post insertedPost = context
                    .Posts
                    .Add(postToInsert)
                    .Entity;

                context.SaveChanges();

                return insertedPost;
            }
            catch (Exception)
            {
                return null;
            }
            

        }

        public Post GetPostById(string postId)
        {
            try
            {
                return context
                    .Posts
                    .Include(p => p.ByUser)
                    .Include(p => p.Comments)
                    .Where(p => p.Id.ToString() == postId)
                    .FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        public IEnumerable<Post> GetPostsByStatus(PostStatus status)
        {
            try
            {
                return context
                    .Posts
                    .Include(p => p.ByUser)
                    .Include(p => p.Comments)
                    .ThenInclude(c => c.ByUser)
                    .Where(posts => posts.Status == status);
            }
            catch (Exception)
            {
                return new Post[0];
            }
            
        }

        public User GetUserById(string userId)
        {
            try
            {
                return context
                    .Users
                    .Where(u => u.Id.ToString() == userId)
                    .FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        public IEnumerable<Post> GetUserPosts(string userId)
        {
            try
            {
                return context
                    .Posts
                    .Include(p => p.ByUser)
                    .Include(p => p.Comments)
                    .ThenInclude(c => c.ByUser)
                    .Where(p => p.ByUser.Id.ToString() == userId);
            }
            catch (Exception)
            {
                return new Post[0];
            }
            
        }

        public bool UpdatePost(string postId, string title, string content)
        {
            try
            {
                Post post = context
                .Posts
                .FirstOrDefault(p => p.Id.ToString() == postId);

                if (post == null) return false;

                if (!string.IsNullOrEmpty(title))
                    post.Title = title;

                if (!string.IsNullOrEmpty(content))
                    post.Content = content;

                context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            

            return true;
        }

        public bool UpdatePostStatus(string postId, PostStatus status)
        {
            try
            {
                Post post = context
                .Posts
                .FirstOrDefault(p => p.Id.ToString() == postId);

                if (post == null) return false;

                if (post.Status == status) return true;

                post.Status = status;

                context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            

            return true;
        }

        public bool UpdatePostStatus(string postId, string byUserId, bool approve, string rejectionMessage = null)
        {
            try
            {
                Post post = context
                .Posts
                .FirstOrDefault(p => p.Id.ToString() == postId);

                if (post == null) return false;

                post.Status = approve ? PostStatus.Published : PostStatus.Rejected;
                post.PublishDate = approve ? DateTime.Now : default;

                if (!approve && !string.IsNullOrEmpty(rejectionMessage))
                {
                    User user = GetUserById(byUserId);

                    if (user == null) return false;

                    Comment comment = new Comment
                    {
                        Content = rejectionMessage,
                        Date = DateTime.Now,
                        ByUser = user,
                        Type = CommentType.Rejection
                    };

                    context.Comments.Add(comment);

                    if (post.Comments == null)
                        post.Comments = new List<Comment>();

                    post.Comments.Add(comment);
                }

                context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }
            

            return true;
        }
    }
}
